#tool nuget:?package=NUnit.ConsoleRunner&version=3.9.0
#tool nuget:?package=JetBrains.ReSharper.CommandLineTools&version=2018.3.4

#addin nuget:https://api.nuget.org/v3/index.json?package=Cake.Npx&version=1.3.0
#addin nuget:https://api.nuget.org/v3/index.json?package=Cake.Issues&version=0.6.2
#addin nuget:https://api.nuget.org/v3/index.json?package=Cake.Issues.InspectCode&version=0.6.1
#addin nuget:https://api.nuget.org/v3/index.json?package=Cake.FileHelpers&version=3.1.0

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

// Define directories.
string projectName;
string projectPath;
DirectoryPath buildDir;
FilePath solutionFile;

// Error rules for resharper
// Example: {"InconsistentNaming", "RedundantUsingDirective"};
string [] resharperErrorRules = {};

Action<NpxSettings> requiredSemanticVersionPackages = settings => settings
    .AddPackage("semantic-release")
    .AddPackage("@semantic-release/commit-analyzer")
    .AddPackage("@semantic-release/release-notes-generator")
    .AddPackage("@semantic-release/gitlab")
    .AddPackage("@semantic-release/git")
    .AddPackage("@semantic-release/exec")
    .AddPackage("conventional-changelog-eslint");

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Get-Project-Name")
    .Does(() =>
{
    var solutions = GetFiles("./**/*.sln");
    projectName = solutions.First().GetFilenameWithoutExtension().ToString();
    Information("Project Name: {0}", projectName);
	
    solutionFile = solutions.First().ToString();
    Information("Solution File: {0}", solutionFile.ToString());
	
    projectPath = Context.Environment.WorkingDirectory.ToString().ToString() + "/src";
    Information("Project Directory: {0}", projectPath);
	
    buildDir = Directory(projectPath + "/" + projectName + "/bin") + Directory(configuration);
    Information("Build Directory: {0}", buildDir.ToString());
});

Task("Clean")
    .IsDependentOn("Get-Project-Name")
    .Does(() =>
{
    CleanDirectory(buildDir);
    CleanDirectory("./dist");
});

Task("Restore-NuGet-Packages")
    .IsDependentOn("Get-Project-Name")
    .Does(() =>
{
    NuGetRestore(solutionFile);
});

Task("Resharper")
    .IsDependentOn("Get-Project-Name")
    .Does(() =>
{
    FilePath dupLog = Context.Environment.WorkingDirectory + "/Resharper/dupfinder.xml";
    FilePath inspectLog = Context.Environment.WorkingDirectory + "/Resharper/inspectcode.xml";
    
    DupFinder(solutionFile, new DupFinderSettings() {
        OutputFile = dupLog.ToString()
    });
    
    Information("DupFinder Log:{0}{1}", Environment.NewLine, FileReadText(dupLog));
    
    InspectCode(solutionFile, new InspectCodeSettings() {
        OutputFile = inspectLog.ToString()
    });
    
    var issues = ReadIssues(
        InspectCodeIssuesFromFilePath(inspectLog.ToString()),
        Context.Environment.WorkingDirectory);

    Information("{0} issues are found.", issues.Count());
    
    Information("InspectCode Log:{0}{1}", Environment.NewLine, FileReadText(inspectLog));
    
    var errorIssues = issues.Where(issue => resharperErrorRules.Any(issue.Rule.Contains)).ToList();
 
    if(errorIssues.Any())
    {
        var errorMessage = errorIssues.Aggregate(new StringBuilder(), (stringBuilder, issue) => stringBuilder.AppendFormat("FileName: {0} Line: {1} Message: {2}{3}", issue.AffectedFileRelativePath, issue.Line, issue.Message, Environment.NewLine));
        throw new CakeException($"{errorIssues.Count} errors detected: {Environment.NewLine}{errorMessage}.");
    }
});
                
Task("Update-Assembly-Info")
    .IsDependentOn("Get-Project-Name")
    .Does(() =>
{
    
    Information("Running semantic-release in dry run mode to extract next semantic version number");

    string[] semanticReleaseOutput;
    Npx("semantic-release", "--dry-run", requiredSemanticVersionPackages, out semanticReleaseOutput);

    Information(string.Join(Environment.NewLine, semanticReleaseOutput));

    var nextSemanticVersionNumber = ExtractNextSemanticVersionNumber(semanticReleaseOutput);

    if (nextSemanticVersionNumber == null) {
        Warning("There are no relevant changes. AssemblyInfo won't be updated!");
    } else {
        Information("Next semantic version number is {0}", nextSemanticVersionNumber);
        
        var assemblyVersion = $"{nextSemanticVersionNumber}.0";
            
        CreateAssemblyInfo(projectPath + "/" + projectName + "/Properties/AssemblyInfo.cs", new AssemblyInfoSettings{
            Product = projectName,
            Title = projectName,
            Company = "RWTH Aachen University IT Center",
            Version = assemblyVersion,
            FileVersion = assemblyVersion,
            InformationalVersion  = assemblyVersion,
            Copyright = "RWTH Aachen University IT Center " + DateTime.Now.Year
        });
    }
});
                
Task("Build-Release")
    .IsDependentOn("Get-Project-Name")
    .Does(() =>
{        
    if(IsRunningOnWindows())
    {
        // Use MSBuild
        MSBuild(solutionFile, settings => 
        {
            settings.SetConfiguration(configuration);
            settings.WithProperty("DebugSymbols", "false");
            settings.WithProperty("DebugType", "None");
        });
    }
    else
    {
        // Use XBuild
        XBuild(solutionFile, settings =>
        {            
            settings.SetConfiguration(configuration);
            settings.WithProperty("DebugSymbols", "false");
            settings.WithProperty("DebugType", "None");
        });
    }
    CopyDirectory(buildDir, "./dist");
});

Task("Build")
    .IsDependentOn("Get-Project-Name")
    .Does(() =>
{        
    if(IsRunningOnWindows())
    {
        // Use MSBuild
        MSBuild(solutionFile, settings => 
        {
            settings.SetConfiguration(configuration);
            settings.WithProperty("RunCodeAnalysis", "true");
        });
    }
    else
    {
        // Use XBuild
        XBuild(solutionFile, settings =>
        {            
            settings.SetConfiguration(configuration);
            settings.WithProperty("RunCodeAnalysis", "true");
        });
    }
});

Task("Run-Unit-Tests")
    .Does(() =>
{
    NUnit3("./src/**/bin/" + configuration + "/*.Tests.dll", new NUnit3Settings {
        // generate the xml file
        NoResults = false,
        Results = new NUnit3Result[]{new NUnit3Result(){
            FileName = Context.Environment.WorkingDirectory + "/TestResult.xml",
            Transform = Context.Environment.WorkingDirectory + "/nunit3-junit.xslt"
        }}
    });
});

Task("Semantic-Release")
    .Does(() =>
{
    Npx("semantic-release", requiredSemanticVersionPackages);
});

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Clean")
    .IsDependentOn("Restore-NuGet-Packages")
    .IsDependentOn("Resharper")
    .IsDependentOn("Build")
    .IsDependentOn("Run-Unit-Tests");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////

string ExtractNextSemanticVersionNumber(string[] semanticReleaseOutput)
{
    var extractRegEx = new System.Text.RegularExpressions.Regex("^.+next release version is (?<SemanticVersionNumber>.*)$");

    return semanticReleaseOutput
        .Select(line => extractRegEx.Match(line).Groups["SemanticVersionNumber"].Value)
        .Where(line => !string.IsNullOrWhiteSpace(line))
        .SingleOrDefault();
}